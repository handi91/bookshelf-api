/**
 * Kumpulan fungsi untuk menghandle request dari user
 */

const { nanoid } = require('nanoid'); // nanoid dipakai untuk auto-generate book id
const books = require('./books'); // import array books dari books.js

/**
 * Fungsi addBook melakukan handle terhadap request user yang ingin menyimpan book
 */
const addBook = (request, h) => {
  const {
    name,
    year,
    author,
    summary,
    publisher,
    pageCount,
    readPage,
    reading,
  } = request.payload;

  // validasi properti name pada body request
  if (name === undefined) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal menambahkan buku. Mohon isi nama buku',
    });
    response.code(400);
    return response;
  }

  // validasi properti readPage dan pageCount pada body request
  if (readPage > pageCount) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount',
    });
    response.code(400);
    return response;
  }

  // assign data yang belum ada
  const id = nanoid(16); // auto-generate book id
  const insertedAt = new Date().toISOString();
  const updatedAt = insertedAt;
  const finished = pageCount === readPage;

  // instansiasi book yang akan disimpan
  const bookToSave = {
    id,
    name,
    year,
    author,
    summary,
    publisher,
    pageCount,
    readPage,
    finished,
    reading,
    insertedAt,
    updatedAt,
  };

  books.push(bookToSave); // menyimpan book ke dalam array books

  // Validasi status keberhasilan penyimpanan
  const isSuccess = books.filter((book) => book.id === id).length > 0;

  // response body yang akan dikirim jika request berhasil terpenuhi
  if (isSuccess) {
    const response = h.response({
      status: 'success',
      message: 'Buku berhasil ditambahkan',
      data: {
        bookId: id,
      },
    });
    response.code(201);
    return response;
  }

  // response body yang dikirim jika request gagal dipenuhi akibat generic error
  const response = h.response({
    status: 'error',
    message: 'Buku gagal ditambahkan',
  });
  response.code(500);
  return response;
};

/**
 * Fungsi getBooks menampilkan daftar seluruh book maupun book berdasarkan kriteria pada query
 */
const getBooks = (request, h) => {
  const { name, reading, finished } = request.query;

  const filteredBook = books.filter((book) => {
    if (name) { // filter berdasarkan nilai name di query
      const substringName = new RegExp(name, 'i'); // RegExp digunakan untuk mengecek keberadaan substring
      if (!substringName.test(book.name)) {
        return false; // jika book.name tidak mengandung substringName artinya tidak match
      }
    }

    if (reading) { // filter berdasarkan nilai reading di query
      if (Number(book.reading) !== Number(reading)) {
        return false; // jika nilai book.reading tidak sama dengan reading di query maka tidak match
      }
    }

    if (finished) { // filter berdasarkan nilai finished di query
      if (Number(book.finished) !== Number(finished)) {
        return false; // jika nilai book.finished tidak sama dengan finished query maka tidak match
      }
    }

    return true; // jika lolos filter atau query tidak di-define maka akan match
  });

  // response body yang akan dikirim ketika request terpenuhi
  const response = h.response({
    status: 'success',
    data: {
      books: filteredBook.map((book) => ({
        id: book.id,
        name: book.name,
        publisher: book.publisher,
      })),
    },
  });
  response.code(200);
  return response;
};

/**
 * Fungsi getBookById menampilkan detail book berdasarkan id params
 */
const getBookById = (request, h) => {
  const { id } = request.params;

  // Melakukan pencarian objek book pada array books
  const book = books.filter((bk) => bk.id === id)[0];

  // response body yang dikirim jika buku not found
  if (book === undefined) {
    const response = h.response({
      status: 'fail',
      message: 'Buku tidak ditemukan',
    });
    response.code(404);
    return response;
  }

  // response body yang akan dikirim jika buku ditemukan
  const response = h.response({
    status: 'success',
    data: {
      book,
    },
  });
  response.code(200);
  return response;
};

/**
 * Fungsi updateBook melakukan update data book berdasarkan id params
 */
const updateBook = (request, h) => {
  const { id } = request.params;
  const {
    name,
    year,
    author,
    summary,
    publisher,
    pageCount,
    readPage,
    reading,
  } = request.payload;

  // validasi properti name pada body request
  if (name === undefined) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal memperbarui buku. Mohon isi nama buku',
    });
    response.code(400);
    return response;
  }

  // validasi properti readPage dan pageCount pada body request
  if (readPage > pageCount) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount',
    });
    response.code(400);
    return response;
  }

  const updatedAt = new Date().toISOString();
  const index = books.findIndex((book) => book.id === id);

  // book ditemukan dan di-update kemudian dikirimkan response body setelah book sukses terupdate
  if (index !== -1) {
    books[index] = {
      ...books[index],
      name,
      year,
      author,
      summary,
      publisher,
      pageCount,
      readPage,
      reading,
      updatedAt,
    };

    const response = h.response({
      status: 'success',
      message: 'Buku berhasil diperbarui',
    });
    response.code(200);
    return response;
  }

  // response body yang akan dikirim jika book not found
  const response = h.response({
    status: 'fail',
    message: 'Gagal memperbarui buku. Id tidak ditemukan',
  });
  response.code(404);
  return response;
};

/**
 * Fungsi deleteBook melakukan penghapusan book yang tersimpan berdasarkan id params
 */
const deleteBook = (request, h) => {
  const { id } = request.params;

  const index = books.findIndex((book) => book.id === id);

  // book ditemukan dan dihapus kemudian akan dikirimkan response body setelah sukses dihapus
  if (index !== -1) {
    books.splice(index, 1);
    const response = h.response({
      status: 'success',
      message: 'Buku berhasil dihapus',
    });
    response.code(200);
    return response;
  }

  // response body yang dikirim jika book not found
  const response = h.response({
    status: 'fail',
    message: 'Buku gagal dihapus. Id tidak ditemukan',
  });
  response.code(404);
  return response;
};

module.exports = {
  addBook,
  getBooks,
  getBookById,
  updateBook,
  deleteBook,
};
